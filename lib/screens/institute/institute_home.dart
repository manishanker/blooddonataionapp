import 'package:flutter/material.dart';
import '../../style.dart';

class InstituteHomePage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
  
  final requestDonorsButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {},
        child: Text("Request Nearby Donors",
            textAlign: TextAlign.center,
            style: Body1Style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

      final logoutButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
            Navigator.pop(context);
        },
        child: Text("Logout",
            textAlign: TextAlign.center,
            style: Body1Style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );


    return Scaffold(
      body: SingleChildScrollView(
      child: Center(
        child: Container(
          color: Colors.lightBlue[50],
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 150.0),
                requestDonorsButton,
                SizedBox(height: 55.0),
                logoutButon,
                SizedBox(height: 55.0),
                SizedBox(height: 200.0),
              ],
            ),
          ),
        ),
      ),
        )
    );
  }

}